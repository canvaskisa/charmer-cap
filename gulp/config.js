var path = require('path'),
    src = path.join(__dirname, '..', 'src'),
    dest = path.join(__dirname, '..', 'out');

module.exports = {
  
  js: [{
    browserify: {
      cache: {},
      packageCache: {},
      fullPaths: false,
      entries: path.join(src, 'js/app.js'),
      extensions: ['.js', '.json', '.jsx', '.babel']
    },
    dest: path.join(src, 'handlebars', '_'),
    source: 'app.js.handlebars'
  }],

  markup: {
    hbsOptions: {
      batch: [
        './src/handlebars/_',
        './src/handlebars/sections'
      ],
      ignorePartials: true,
      helpers : {
        ifeq: function(v1, v2, options) {
          if(v1 === v2) {
            return options.fn(this);
          }
          return options.inverse(this);
        },
        json: function(json) {
          return JSON.stringify(json);
        },
        length: function(arr) {
          return arr.length;
        }
      }
    },
    htmlmin: {
      minifyJS: true, 
      collapseWhitespace: true,
      minifyCSS: true
    },
    data: path.join(src, 'data.json'),
    src: path.join(src, 'handlebars', 'index.handlebars'),
    dest: dest
  },

  copy: [{
    src: path.join(src, 'fonts/**'),
    dest: path.join(dest, 'fonts')
  }],

  css: [{
    src: path.join(src, 'scss/style.scss'),
    dest: path.join(src, 'handlebars', '_'),
    prefixer: {
      browsers: ['> 0%'],
      cascade: false
    },
    rename: 'style.css.handlebars'
  }],

  browserSync: {
    server: {
      baseDir: dest
    },
    files: [
      path.join(dest, 'index.html')
    ],
    port: 3000,
    open: false
  },

  watch: [{
    path: path.join(src, 'scss/**'),
    name: ['scss']
  }, {
    path: path.join(src, '**/*.handlebars'),
    name: ['markup']
  }],

  lint: {
    scss: {
      src: [
        path.join(src, 'scss/**/*.scss'),
        path.join('!', src, 'scss/helpers/**/*.scss'),
        path.join('!', src, 'scss/base/**/*.scss'),
      ],
      dest: 'scss-report.xml',
      config: path.join(__dirname, '..', 'scss-lint.yml')
    },
    js: {}
  }
};
