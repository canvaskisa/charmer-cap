var gulp = require('gulp'),
    gutil = require('gulp-util');

gulp.task('build', ['markup', 'copy'], function() {
  gutil.log(gutil.colors.green('Build done without errors!'));
});
