var gulp = require('gulp'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync'),
    config = require('../config').watch,
    syncConfig = require('../config').browserSync;

gulp.task('default', function() {
  browserSync(syncConfig); // set browserSync config

  config.forEach(function(config) { // watch for file changing and start task
    watch(config.path, function() {
      gulp.start(config.name);
    });
  });

  global.isWatchify = true; // set watchify listening
  gulp.start('js'); // start js task without watch, cause of watchify
});
