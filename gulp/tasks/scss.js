var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssshrink = require('gulp-cssshrink'),
    handleErrors = require('../util/handleErrors'),
    autoprefixer = require('gulp-autoprefixer'),
    minimist = require('minimist'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    config = require('../config').css,
    def, options;

def = {
  string: 'env',
  default: {
    env: process.env.NODE_ENV || 'dev'
  }
};
options = minimist(process.argv.slice(2), def);

gulp.task('scss', function() {
  return config.map(function(config) {
    return gulp.src(config.src) // get src from config
      .pipe(sass()) // precompile sass
      .on('error', handleErrors) // handle errors
      .pipe(autoprefixer(config.prefixer)) // autoprefix postprocessor
      .pipe(gulpif(options.env === 'prod', cssshrink())) // minify if production
      .pipe(rename(config.rename))
      .pipe(gulp.dest(config.dest)); // write to destination
  });
});
