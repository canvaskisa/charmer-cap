'use strict';
import * as lib from './lib';

const BREAKPOINT = 1400;

/**
 * Scrollable container
 */
(() => {

  /**
   * Settings and variables
   */
  let triggers = lib.querySelectorAll('.js-anchor').map((el) => { 
    return {
      el, 
      target: el.getAttribute('href').replace('#', ''),
      handler: void 0
    };
  });

  const sections = lib.querySelectorAll('.js-section').map((el, i) => {
    return {
      el,
      id: el.getAttribute('id'),
      i
    };
  });

  const doc = document.documentElement;
  const DEBOUNCE_SPEED = 1200;
  const TRIGGER_ACTIVE_CLASS = 'active';
  const container = document.querySelector('.js-container');
  const transform = lib.getSupportedCSSProperty(['transform', 'MozTransform', 'WebkitTransform', 'msTransform', 'OTransform']);
  const length = sections.length;
  let WH = window.innerHeight;
  let active = 0;
  let activeId = 'preview';
  let xDown, yDown;
  let timeout;
  let deviceType;

  /**
   * Desktop functions
   */
  const setContainerTransform = () => {
    container.style[transform] = 'translateY(' + (-1 * active * WH) + 'px) translateZ(0) translate3d(0, 0, 0)';
  };

  const setActiveTriggers = () => {
    triggers.forEach((trigger) => {
      trigger.target === activeId 
      ? lib.addClass(trigger.el, TRIGGER_ACTIVE_CLASS) 
      : lib.removeClass(trigger.el, TRIGGER_ACTIVE_CLASS)
    });
  };

  const onTrigger = (trigger, e) => {
    e.preventDefault();
    const { id, i } = sections.filter((section) => trigger.target === section.id)[0];
    activeId = id;
    active = i;
    setContainerTransform();
    setActiveTriggers();
  };

  const onUserInteract = (e, delta) => {
    e.preventDefault();
    active = delta === 1 ? active - 1 : active + 1;
    if (active < 0) active = 0;
    if (active > length - 1 ) active = length -1;
    activeId = sections[active].id;
    setContainerTransform();
    setActiveTriggers();
  };

  const onMouseWheel = (e) => {
    const delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    e.preventDefault();

    if (!timeout) {
      timeout = setTimeout(() => timeout = void 0, DEBOUNCE_SPEED);
      onUserInteract(e, delta);
    }
    
  };

  const handleTouchStart = (e) => {
    xDown = e.touches[0].clientX;
    yDown = e.touches[0].clientY;
    e.preventDefault(); 
  };

  const handleTouchMove = (e, callback) => {
    if (!xDown || !yDown) return void 0;

    const xUp = e.touches[0].clientX;
    const yUp = e.touches[0].clientY;
    const xDiff = xDown - xUp;
    const yDiff = yDown - yUp;

    if (Math.abs(xDiff) < Math.abs(yDiff)) {
      if (yDiff > 0) {
        onUserInteract(e, -1); // up
      } else { 
        onUserInteract(e, 1); // down
      }                                                                 
    }

    xDown = void 0;
    yDown = void 0;  
    e.preventDefault();                                          
  };

  const onDesktopWindowResize = () => {
    WH = window.innerHeight;
    setContainerTransform();
  };

  const setBodyOverflow = (method) => {
    const OVERFLOW_HIDDEN_CLASS = 'd-overflow--hidden';
    lib[method](document.body, OVERFLOW_HIDDEN_CLASS); // Disable scroll
    window.scrollTo(0, 0); // Scroll to top, you know
  };

  /**
   * Mobile functions
   */
  const slideToSection = () => {
    const scrollTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
    const top = sections[active].el.getBoundingClientRect().top;
    lib.animate(350, 'ease', (percent) => window.scrollTo(0, scrollTop + (top * percent)));
  };

  const onMobileTrigger = (trigger, e) => {
    e.preventDefault();
    const { id, i } = sections.filter((section) => trigger.target === section.id)[0];
    activeId = id;
    active = i;
    slideToSection();
  };

  const onMobileScroll = () => {
    const scrollTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
    sections.forEach((section) => {
      const rect = section.el.getBoundingClientRect();
      const top = rect.top;
      const topPos = top + scrollTop - window.innerHeight / 2;

      if (scrollTop >= topPos) {
        active = section.i;
        activeId = section.id;
      }
    });

    setActiveTriggers();
  };

  /**
   * Handlers
   */
  const setDesktopHandlers = () => {
    triggers = triggers.map((trigger) => {
      const handler = onTrigger.bind(void 0, trigger);
      trigger.el.addEventListener('click', handler, false);
      return {
        el: trigger.el,
        target: trigger.target,
        handler: handler
      };
    });
    lib.handleSwipe(container, handleTouchStart, handleTouchMove);
    lib.handleMouseWheel(container, onMouseWheel);
    window.addEventListener('resize', onDesktopWindowResize, false);
  };

  const removeDesktopHandlers = () => {
    triggers = triggers.map((trigger) => {
      trigger.el.removeEventListener('click', trigger.handler, false);
      return {
        el: trigger.el,
        target: trigger.target,
        handler: void 0
      };
    });
    lib.removeSwipe(container, handleTouchStart, handleTouchMove);
    lib.removeMouseWheel(container, onMouseWheel);
    window.removeEventListener('resize', onDesktopWindowResize, false);
  };

  const setMobileHandlers = () => {
    triggers = triggers.map((trigger) => {
      const handler = onMobileTrigger.bind(void 0, trigger);
      trigger.el.addEventListener('click', handler, false);
      return {
        el: trigger.el,
        target: trigger.target,
        handler
      };
    });
    window.addEventListener('scroll', onMobileScroll, false);
  };

  const removeMobileHandlers = () => {
    triggers = triggers.map((trigger) => {
      trigger.el.removeEventListener('click', trigger.handler, false);
      return {
        el: trigger.el,
        target: trigger.target,
        handler: void 0
      };
    });
    window.removeEventListener('scroll', onMobileScroll, false);
  };

  const onResize = () => {
    if (window.innerWidth > BREAKPOINT && deviceType === 'mobile' || !deviceType) {
      removeMobileHandlers();
      setDesktopHandlers();
      setBodyOverflow('addClass');
      deviceType = 'desktop';
    } 

    if (window.innerWidth < BREAKPOINT && deviceType === 'desktop' || !deviceType) {
      removeDesktopHandlers();
      setMobileHandlers();
      setBodyOverflow('removeClass');
      container.style[transform] = 'translateY(0px) translateZ(0) translate3d(0, 0, 0)';
      deviceType = 'mobile';
    }
  };

  onResize();
  window.addEventListener('resize', onResize, false);

})();

/**
 * Icon
 */
(() => {
  const STROKE_SPEED = 10000;
  const OPACITY_SPEED = 3000;
  const icon = document.querySelector('.js-icon');
  let paths = lib.querySelectorAll('path', icon);

  paths = paths.map((path) => {
    const length = path.getTotalLength();
    path.setAttribute('stroke-dashoffset', length * Math.random());
    path.setAttribute('stroke-dasharray', [length, length].join(' '));
    path.setAttribute('opacity', Math.random());

    return {
      node: path,
      length,
      strokeSpeed: STROKE_SPEED * (Math.random() + 1),
      opacitySpeed: OPACITY_SPEED * (Math.random() + 1),
      startTime: void 0
    };
  });

  const loop = () => {
    setTimeout(() =>  requestAnimationFrame(loop), 60);
    animate();
  };

  const start = () => {
    paths.forEach((path) => path.startTime = Date.now());
    loop();
  };

  const animate = () => {
    paths.forEach((path) => {
      const now = Date.now();
      const diff = now - path.startTime;

      path.node.setAttribute('stroke-dashoffset', path.length * diff / path.strokeSpeed);
      path.node.setAttribute('opacity', Math.abs(Math.sin(diff / path.opacitySpeed)));

    });
  };

  start();

})();
