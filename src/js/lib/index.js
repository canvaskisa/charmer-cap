'use strict';
import bezierEasing from './bezier';
import polyfills from './polyfills';
const __slice = Array.prototype.slice;

export function hasClass(el, className) {
  return el.className.indexOf(className) === -1 ? false : true;
};

export function addClass(el, className) {
  if (!hasClass(el, className)) el.className += ' ' + className;
};

export function removeClass(el, className) {
  el.className = el.className.replace(className, '');
};

export function querySelectorAll(val, el) {
  return el 
    ? __slice.call(el.querySelectorAll(val))
    : __slice.call(document.querySelectorAll(val));
};

/**
 * Checks if browser supports css property
 * @param {Array} array of properties you wanna check
 * example: ['transform', 'MozTransform', 'WebkitTransform', 'msTransform', 'OTransform']
 * @return {String} string of supported property
 */
export function getSupportedCSSProperty(arr) {
  const doc = document.documentElement;
  const len = arr.length;

  for (let i = 0; i < len; i++) {
    if (arr[i] in doc.style) return arr[i]
  }
};

export function handleMouseWheel(el, callback) {
  el.addEventListener('mousewheel', callback, false);
  el.addEventListener('DOMMouseScroll', callback, false);
};

export function handleSwipe(el, touchStart, touchMove) {
  el.addEventListener('touchstart', touchStart, false);        
  el.addEventListener('touchmove', touchMove, false);
};

export function removeSwipe(el, touchStart, touchMove) {
  el.removeEventListener('touchstart', touchStart, false);        
  el.removeEventListener('touchmove', touchMove, false);
};

export function removeMouseWheel(el, callback) {
  el.removeEventListener('mousewheel', callback, false);
  el.removeEventListener('DOMMouseScroll', callback, false);
};

export function animate(speed, easing, callback) {
  let startTime = Date.now();
  let raf;

  (function loop() {
    let now = Date.now();
    let diff = now - startTime;
    let percent = diff / speed;

    if (percent > 1) {
      callback(1);
      cancelAnimationFrame(raf);
    } else {
      callback(bezierEasing.css[easing](percent));
      raf = requestAnimationFrame(loop.bind(this));
    }
  })();
};
